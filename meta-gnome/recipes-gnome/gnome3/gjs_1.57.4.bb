DESCRIPTION = "GNOME JavaScript engine"
LICENSE = "MIT|LGPLv2+"
DEPENDS = " \
            glib-2.0 \
            gsettings-desktop-schemas \
            mozjs \
            cairo \
          "

LIC_FILES_CHKSUM = "file://COPYING;md5=beb29cf17fabe736f0639b09ee6e76fa"

SRC_URI = " \
    https://gitlab.gnome.org/GNOME/gjs/-/archive/${PV}/${PN}-${PV}.tar.gz \
"

SRC_URI[md5sum] = "3392eb2a0e9a533ecc98f159b5f3bfeb"
SRC_URI[sha256sum] = "3b4d3ac0d7c83fe4006cc94dc5fbd660b8d1698f52d1055aec0bfb09b0439b81"

inherit autotools pkgconfig gobject-introspection
EXTRA_OECONF += " --without-dbus-tests "

FILES_${PN} += "${datadir}"

do_configure_prepend() {
	export GI_DATADIR="${RECIPE_SYSROOT_NATIVE}${datadir}/gobject-introspection-1.0"
}

