DESCRIPTION = "GNOME Settings Daemon"
LICENSE = "GPLv2"
DEPENDS = " \
    ${@bb.utils.contains("DISTRO_FEATURES", "wayland", "wayland", "", d)} \
    ${@bb.utils.contains("DISTRO_FEATURES", "wayland", "wayland-protocols", "", d)} \
    libcanberra \
    prelink \
    glib-2.0 \
    gsettings-desktop-schemas \
    gnome-desktop3 \
    gettext-native \
    polkit \
    upower \
    libnotify \
    geocode-glib \
    libgweather \
    lcms \
    libwacom \
    networkmanager \
    geoclue \
"

RDEPENDS_${PN} = " \
    geoclue \
    geocode-glib \
    libgweather \
"

LIC_FILES_CHKSUM = "file://COPYING;md5=59530bdf33659b29e73d4adb9f9f6552"

SRC_URI = " \
    https://download.gnome.org/core/3.30/3.30.2/sources/${PN}-${PV}.tar.xz \
    file://0001-remove-extraneous-plugins.patch \
"

SRC_URI[md5sum] = "bdac5b7329f919f7172f1feb240e48d8"
SRC_URI[sha256sum] = "5cdcf64f6e41fe1816e719850709e8e7a53a11460955f2be77fcee9c80812d1d"

inherit pkgconfig meson gobject-introspection distro_features_check

EXTRA_OEMESON += " -Denable-gtk-doc=false -Dcups=false "

do_configure_prepend() {
	# Fixup the gsettings version
	sed -i 's^3.27.90^3.24.1^g' ${S}/meson.build
}

FILES_${PN} += " \
    ${datadir} \
    ${libdir}/${PN}-3.0 \
"

