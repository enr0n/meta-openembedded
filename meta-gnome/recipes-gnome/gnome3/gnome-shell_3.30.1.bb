DESCRIPTION = "GNOME Shell"
LICENSE = "GPLv2"
DEPENDS = " \
    ${@bb.utils.contains("DISTRO_FEATURES", "wayland", "wayland", "", d)} \
    ${@bb.utils.contains("DISTRO_FEATURES", "wayland", "wayland-protocols", "", d)} \
    evolution-data-server \
    gdm \
    gjs \
    glib-2.0 \
    gnome-bluetooth \
    gnome-desktop3 \
    gnome-settings-daemon \
    gsettings-desktop-schemas \
    gstreamer1.0 \
    ibus \
    keybinder \
    libcroco \
    librsvg \
    libxslt-native \
    mutter \
    polkit \
    sassc-native \
    webkitgtk \
"

LIC_FILES_CHKSUM = "file://COPYING;md5=b234ee4d69f5fce4486a80fdaf4a4263"

SRC_URI = " \
    https://download.gnome.org/core/3.30/3.30.2/sources/${PN}-${PV}.tar.xz \
    file://0001-dont-detect-python.patch \
    file://0002-allow-deprecated-declarations.patch \
    file://0003-build-params-dependency.patch \
"

SRC_URI[md5sum] = "9ff4d30d2ee8a9fd84932b496254b63f"
SRC_URI[sha256sum] = "fcdf879781b86a88ad0430e84e4d882cf7b3ac26af9f16f666a980b8f96c5fe1"

RDEPENDS_${PN} += " \
    at-spi2-core \
    gdm \
    gnome-bluetooth \
    gnome-desktop3 \
    gnome-settings-daemon \
    ibus \
    iso-codes \
    librsvg-gtk \
    python3-core \
    python3-pygobject \
"

inherit pkgconfig meson gobject-introspection gettext manpages distro_features_check

EXTRA_OEMESON += " -Dman=false -Dnetworkmanager=false "

do_configure_prepend() {
	# Fixup the gsettings version
	sed -i 's^3.27.90^3.24.1^g' ${S}/meson.build
	sed -i "s^@PYTHON3_PATH@^${RECIPE_SYSROOT_NATIVE}/${bindir}/python3^g" ${S}/meson.build
}

FILES_${PN} += " \
    ${libdir} \
    ${datadir} \
"

SYSTEMD_SERVICE_${PN} = "${PN}.service"
