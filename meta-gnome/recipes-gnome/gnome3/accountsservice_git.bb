DESCRIPTION = "Accounts Service"
LICENSE = "GPLv3"
DEPENDS = " \ 
    glib-2.0 \
    gsettings-desktop-schemas \
    polkit \
    dbus \
"

LIC_FILES_CHKSUM = "file://COPYING;md5=d32239bcb673463ab874e80d47fae504"

SRC_URI = " \
    git://anongit.freedesktop.org/accountsservice.git;protocol=git;branch=master \
"

# TAG: 0.6.54
SRCREV="708d87f30e549588f7efa03efeffab1c9b83cdcb"

S = "${WORKDIR}/git"

inherit pkgconfig meson gobject-introspection systemd gettext

FILES_${PN} += "${datadir}"

SYSTEMD_SERVICE_${PN} = "accounts-daemon.service"
